import XCTest
@testable import Forms

final class FormsTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(Forms().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
