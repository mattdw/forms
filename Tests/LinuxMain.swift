import XCTest

import FormsTests

var tests = [XCTestCaseEntry]()
tests += FormsTests.allTests()
XCTMain(tests)
